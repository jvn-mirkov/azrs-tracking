# azrs-tracking

Projekat nad kojim su korišćeni alati: 
[Raccoon](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/11-raccoon)

Korišćeni alati:
- Git 
- Git Hooks
- CMake
- GCov
- Clang Static Analyzer
- Gammaray
- Clazy
- Docker
- Valgrind
- ClangFormat
- ClangTidy

